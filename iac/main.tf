terraform {
  backend "s3" {
    bucket         = "tf-state-cloudsanatomy"
    key            = "terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "terraform-lock"
    encrypt        = true
  }
}