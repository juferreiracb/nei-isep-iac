module "vpc" {
  source = "./modules/terraform-aws-vpc"

  name = "${var.customer}-vpc"

  cidr = var.vpc_cidr

  azs             = var.azs
  private_subnets = var.private_subnets_cidrs
  public_subnets  = var.public_subnets_cidrs

  enable_ipv6 = false
  enable_nat_gateway = true

  single_nat_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Environment = var.environment
  }

  vpc_tags = {
    Name = "${var.customer}-vpc"
  }

}

resource "aws_ssm_parameter" "vpd-id-parameter-store-entry" {
  name  = "/vpc/id"
  type  = "String"
  value = module.vpc.vpc_id
  depends_on = [
    module.vpc
  ]
}

resource "aws_ssm_parameter" "private-subnet-a-id-parameter-store-entry" {
  name  = "/vpc/private-subnet-a/id"
  type  = "String"
  value = module.vpc.private_subnets[0]
  depends_on = [
    module.vpc
  ]
}

resource "aws_ssm_parameter" "private-subnet-b-id-parameter-store-entry" {
  name  = "/vpc/private-subnet-b/id"
  type  = "String"
  value = module.vpc.private_subnets[1]
  depends_on = [
    module.vpc
  ]
}