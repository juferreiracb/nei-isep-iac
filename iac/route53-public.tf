resource "aws_route53_zone" "route53-hostedzone-public" {
  name = var.tld

  tags = {
    Environment = var.environment
  }
}