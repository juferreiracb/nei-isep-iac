resource "aws_cloudformation_stack" "iam-asg-role" {
  name = "iam-asg-role"

capabilities = ["CAPABILITY_IAM"]

  template_body = <<STACK
   {"Resources" : {
  "AutoscalingRole": {
    "Type": "AWS::IAM::Role",
    "Properties": {
      "AssumeRolePolicyDocument": {
        "Statement": [
          {
            "Effect": "Allow",
            "Principal": {
              "Service": [
                "application-autoscaling.amazonaws.com"
              ]
            },
            "Action": [
              "sts:AssumeRole"
            ]
          }
        ]
      },
      "Path": "/",
      "Policies": [
        {
          "PolicyName": "service-autoscaling",
          "PolicyDocument": {
            "Statement": [
              {
                "Effect": "Allow",
                "Action": [
                  "application-autoscaling:*",
                  "cloudwatch:DescribeAlarms",
                  "cloudwatch:PutMetricAlarm",
                  "ecs:DescribeServices",
                  "ecs:UpdateService"
                ],
                "Resource": "*"
              }
            ]
          }
        }
      ]
    }
  }
 },
 "Outputs" : {
  "IAMAsgRole" : {
    "Value" : {"Fn::GetAtt" : ["AutoscalingRole", "Arn"] },
    "Export" : {
      "Name" : "IAMAsgRole"
    }
  }
 }
}

STACK
}
