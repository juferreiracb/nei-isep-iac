
module "alb" {
  source  = "./modules/terraform-aws-alb"

 depends_on = [
   module.vpc, aws_security_group.alb-sg, aws_acm_certificate.regional_certificate
 ]

  name = var.alb_name

  load_balancer_type = "application"

  vpc_id             = module.vpc.vpc_id
  subnets            = module.vpc.public_subnets
  security_groups    = [aws_security_group.alb-sg.id]

  target_groups = [
    {
      name_prefix      = "tg-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
      targets = [
      ]
    }
  ]

  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = aws_acm_certificate.regional_certificate.arn
      target_group_index = 0
    }
  ]

  tags = {
    Environment = var.environment
  }
}
resource "aws_security_group" "alb-sg" {
    vpc_id      = module.vpc.vpc_id

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["77.54.243.88/32"]
    }

    egress {
        from_port       = 0
        to_port         = 65535
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }
}

resource "aws_ssm_parameter" "alb-arn-parameter-store-entry" {
  name  = "/alb/arn"
  type  = "String"
  value = module.alb.lb_arn
  depends_on = [
    module.alb
  ]
}

resource "aws_ssm_parameter" "listener-arn-parameter-store-entry" {
  name  = "/alb/listener/arn"
  type  = "String"
  value = module.alb.https_listener_arns[0]
  depends_on = [
    module.alb
  ]
}


resource "aws_route53_record" "route53-record-set-api" {
  zone_id = aws_route53_zone.route53-hostedzone-public.zone_id
  name    = "api.${var.tld}"
  type    = "A"
  alias {
    name                   = module.alb.lb_dns_name
    zone_id                = module.alb.lb_zone_id
    evaluate_target_health = true
  }
  depends_on = [
    module.alb
  ]
}

resource "aws_route53_record" "route53-record-set-frontend" {
  zone_id = aws_route53_zone.route53-hostedzone-public.zone_id
  name    = "neiisep.${var.tld}"
  type    = "A"
  alias {
    name                   = module.alb.lb_dns_name
    zone_id                = module.alb.lb_zone_id
    evaluate_target_health = true
  }
  depends_on = [
    module.alb
  ]
}