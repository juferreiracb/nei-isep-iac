variable "region" {
  default     = "eu-west-1"
}

variable "environment" {
  default     = "hiring"
}

variable "vpc_cidr" {
  default     = "10.100.0.0/16"
}

variable "tld" {
  default     = "cloudsanatomy.ga"
}

variable "wildcard_tld" {
  default     = "*.cloudsanatomy.ga"
}

variable "private_subnet_a_cidr" {
    default = "10.100.1.0/24"
}

variable "private_subnet_b_cidr" {
    default = "10.100.2.0/24"
}

variable "public_subnet_a_cidr" {
    default = "10.100.101.0/24"
}

variable "public_subnet_b_cidr" {
    default = "10.100.102.0/24"
}

variable "customer" {
    default = "cloudsanatomy"
}

variable "private_subnets_cidrs" {
    default = ["10.100.1.0/24", "10.100.2.0/24"]
}

variable "public_subnets_cidrs" {
    default = ["10.100.101.0/24", "10.100.102.0/24"]
}

variable "azs" {
    default = ["eu-west-1a", "eu-west-1b"]
}

variable "ecr_list" {
    default = ["cloudsanatomy"]
}

variable "ecs_cluster_name" {
    default = "stratocaster"
}

variable "alb_name" {
    default = "fender"
}

variable "ecs_instance_image_id" {
    default = "ami-0b11be160d53889ae"
}

variable "fargate-role-name" {
    default = "fargate-execution-role"
}