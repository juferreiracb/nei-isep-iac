resource "aws_ssm_parameter" "alb-listener-rule-priority-cloudsanatomy-parameter-store-entry" {
  name  = "/alb/listener-rule/priority/cloudsanatomy"
  type  = "String"
  value = 1
}