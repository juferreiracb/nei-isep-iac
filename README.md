# GYANT Challenge App

## Assumptions:

 - You should have a registered and valid Domain, as well edit files iac/vars.tf and cloudformation/hiring.json with this domain name:
    - iac/vars.tf:
        - tld
        - wildcard_tld
    - cloudformation/hiring.json:
        - EnvironmentDNSDomain
 - To allow access from a different source IP edit:
    - File iac/alb.tf
        - Parameter "cidr_blocks     = '["77.54.243.88/32"]' - add address separated by comma.
 - In this example, the https://freenom.com/ was used for domain register (cloudsanatomy.ga).
 - During the Terraform execution, listen to Route53 hosted zone creation: you'll need manually setup the NS registers inside your current DNS/Domain host provider.
 - The script don't handle with SSO tools, such Okta.
 - You should be using Linux Operating system and have docker installed as well.

 ---
## tl; dr

Clone or download this repository, and then:

```
docker build -f Ragnarok . -t ragnarok

docker run -v /var/run/docker.sock:/var/run/docker.sock -e AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=YOUR_SECRET_ACCESS_KEY -e AWS_REGION=DESIRED_AWS_REGION -it ragnarok

```

**Required parameters**:

 - AWS_ACCESS_KEY_ID
 - AWS_SECRET_ACCESS_KEY

**Optional parameters**:

 - AWS_REGION _(default: eu-west-1)_

The user should have enough privileges for create the resources.

## Workflow
    1. .aws/credentials setup
    2. Creation of bucket for storage tfstate
    3. Infrastructure provisioning (terraform)
    4. Move tfstate to the previous bucket
    5. ECR Login
    6. Build app
    7. Tag and push docker image to ECR
    8. Deploy app through CloudFormation

## Files added
 - cloudformation/*
 - iac/*
 - Dockerfile
 - Ragnarok
 - setup.sh
 - createCasesNConditions.sh
 - deleteCasesNConditions.sh

## Solution description / Tech Summary

### High level architecture


![gyant](https://user-images.githubusercontent.com/40604006/119330876-a0867680-bc7e-11eb-8bbb-de1429232fe5.jpg)



Resources provisioning are divided in two parts.
The first one is accountable by infrastructure creation, through Terraform.
The second is accountable by application deployment, through AWS CloudFormation.

**The following resources will be provisioned**:
- VPC and your componentes (Route Tables, private subnets, public subnets, single AZ NAT Gateway, ACL and so on)
- Route 53 public hosted zone (according vars.tf variables as mentioned before
- Route 53 recorsets)
- SSL Certificates (in desired region and in eu-east-1, just in case we desire use a CloudFront Distribution in the future)
- ECR and your IAM role
- ALB multi-az and your security group
- Required IAM componentes for the solution and their componentes
- ECS Cluster
- SSM Parameter Store entries, which 'll be used during application deployment

**The environment is currently down for avoid extra charges. Please let me know if you would like to have it Up & Running for tests and validations, and I'll create it again.

---

## TODOs:
 - Create CloudFormation templates for Nested Stacks, making the application deployment YAML easier.
 - Optimize Docker image build (make the image size smaller)
 - Create CloudWatch Alarm automatically
 - Improve the way to use docker inside docker
 - Improve the way to handle with terraform.tfstate
 - Create a script for destroy resources and components automatically
 - Domain name as parameter
 - Improve error handling in setup.sh
 - Create a mongodb
 - Verify warning message showed during CloudFormation Stack creation: "[AwsVpcConfiguration] is not permitted..."
 - Improve security: review roles and policies and use least privilege principle
