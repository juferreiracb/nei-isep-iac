resource "aws_s3_bucket" "tf-state-cloudsanatomy" {
    bucket = "tf-state-cloudsanatomy"

    versioning {
      enabled = true
    }

    lifecycle {
      prevent_destroy = false
    }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

}

resource "aws_dynamodb_table" "terraform-lock-dynamodb-table" {
  name         = "terraform-lock"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

provider "aws" {
  profile    = "aws-vagner"
  region     = "eu-west-1"
  version    = "v2.70.0"
}
