data "aws_route53_zone" "hosted_zone_data" {
  name         = var.tld
  private_zone = false

  depends_on = [
    aws_route53_zone.route53-hostedzone-public
  ]
}

resource "aws_acm_certificate" "regional_certificate" {
  domain_name       = var.tld
  subject_alternative_names = [var.wildcard_tld]
  validation_method = "DNS"

  tags = {
    Environment = var.environment
  }

  depends_on = [
    aws_route53_zone.route53-hostedzone-public
  ]

  lifecycle {
    create_before_destroy = true
  }
}


  resource "aws_route53_record" "acm_validation" {
  for_each = {
    for dvo in aws_acm_certificate.regional_certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.hosted_zone_data.zone_id
}



resource "aws_acm_certificate" "global_certificate" {
  provider = aws.acm
  domain_name = var.tld
  subject_alternative_names = [var.wildcard_tld]
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_ssm_parameter" "acm-ssl-certificate-arn-parameter-store-entry" {
  name  = "/acm/ssl-certificate/arn"
  type  = "String"
  value = aws_acm_certificate.regional_certificate.arn
  depends_on = [
    aws_acm_certificate.regional_certificate
  ]
}