resource "aws_ecs_cluster" "ecs_cluster" {
    name  = var.ecs_cluster_name
    capacity_providers = ["FARGATE_SPOT"]

}

resource "aws_ssm_parameter" "ecs-cluster-name-parameter-store-entry" {
  name  = "/ecs/cluster/name"
  type  = "String"
  value = var.ecs_cluster_name
}