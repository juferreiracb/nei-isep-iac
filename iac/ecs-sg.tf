resource "aws_security_group" "ecs-sg" {
    vpc_id      = module.vpc.vpc_id

    ingress {
        from_port       = 0
        to_port         = 65535
        protocol        = "tcp"
        cidr_blocks     = [var.vpc_cidr]
    }
    egress {
        from_port       = 0
        to_port         = 65535
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }
}

resource "aws_ssm_parameter" "ecs-security-group-id-parameter-store-entry" {
  name  = "/security-group/ecs/id"
  type  = "String"
  value = aws_security_group.ecs-sg.id
  depends_on = [
    aws_security_group.ecs-sg
  ]
}