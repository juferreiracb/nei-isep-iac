resource "aws_ecr_repository" "ecr" {
  for_each = toset( var.ecr_list )
  name                 = each.key
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository_policy" "ecr-policy" {

  depends_on = [aws_ecr_repository.ecr]
  for_each = toset( var.ecr_list )
  repository = each.key
  policy     = <<EOF
  {
    "Version": "2008-10-17",
    "Statement": [
      {
        "Sid": "adds full ecr access to the repository",
        "Effect": "Allow",
        "Principal": "*",
        "Action": [
          "ecr:BatchCheckLayerAvailability",
          "ecr:BatchGetImage",
          "ecr:CompleteLayerUpload",
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetLifecyclePolicy",
          "ecr:InitiateLayerUpload",
          "ecr:PutImage",
          "ecr:UploadLayerPart"
        ]
      }
    ]
  }
  EOF
}
